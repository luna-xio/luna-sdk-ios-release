# Luna SDK iOS
![splash-icon](https://gitlab.com/luna-xio/luna-sdk-ios-release/-/raw/main/luna-logo.png)

Luna SDK is an iOS framework for working with Luna Trackers. For more information check [Luna XIO](https://lunaxio.com) webpage.
​
## Installation

To integrate Luna SDK use [CocoaPods](https://cocoapods.org/) (version 1.11.3 is recommended).

In your **Podfile** you need to make **3 changes**:

```ruby
# 1. Add sources pointing to this repo and the CocoaPods repo
source 'https://gitlab.com/luna-xio/luna-sdk-ios-release'
source 'https://cdn.cocoapods.org/'

use_frameworks!
target 'YOUR_TARGET_NAME' do
    # [YOUR PODS]

    # 2. Add LunaSDK pod
    pod 'LunaSDK'
end

# 3. Add post install script
post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
    end
  end
end
```

Run ```pod install``` so that CocoaPods fetches and links Luna SDK with your project.

## Prerequisites

#### Project settings

Luna SDK requires the **Location updates** [Background Mode](https://developer.apple.com/documentation/xcode/configuring-background-execution-modes) to work properly. It can be configured in the Xcode project, or added manually to the **Info.plist** file:
```
<key>UIBackgroundModes</key>
<array>
    <string>location</string>
</array>
```
#### Runtime - Location

Luna SDK requires **Always** [Location Authorization](https://developer.apple.com/documentation/corelocation/requesting_authorization_to_use_location_services):
```swift
import CoreLocation

CLLocationManager.requestAlwaysAuthorization()
```

You need to add `NSLocationAlwaysUsageDescription` and `NSLocationAlwaysAndWhenInUseUsageDescription` keys explaining always location usage to **Info.plist** file, otherwise the app will crash.

#### Runtime - Bluetooth

`LunaGateway` is using Bluetooth, so you need to add `NSBluetoothAlwaysUsageDescription` key with explaining Bluetooth usage to **Info.plist** file, otherwise the app might crash.

To check if Bluetooth is active you can use `isBluetoothActive` property from `LunaGateway` class.

## Usage

You just need to `start` the Luna Gateway, that's it!

```swift
import LunaSDK

let lunaGateway = Luna.gateway()

func startLunaGateway() {
  lunaGateway.start()
}

func stopLunaGateway() {
  lunaGateway.stop()
}

```

## Custom configuration

When initiating `LunaGateway`, you can pass `LunaConfiguration` parameter as follows:

```swift
let lunaGateway = Luna.gateway(configuration: LunaConfiguration())
```

`LunaConfiguration` allows you to configure:
- `mqttHost` - MQTT router address, e.g. `router-prod.lunaxio.net`.
- `includeGatewayInfo` - default: `true`.
- `debugMode` - default: `false`.
- `placeName` - named location, default: `nil`.
- `grafanaLoggerConfig` - remote logging (optional), `GrafanaLoggerConfig` object, default: `nil`.

`GrafanaLoggerConfig` can be initiated as follows:

```swift
GrafanaLoggerConfig(url: "grafana-loki-url", login: "login", password: "password")
```

### Support

If you need any assistance with the Luna SDK iOS setup, feel free to [contact the developers directly](mailto:mateusz@lunaxio.com). Feedback appriciated!
